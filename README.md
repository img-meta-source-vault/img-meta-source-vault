# IMG Meta Source Vault

In order for the growth and strength of GDN members, first, this repo prioritises assets made by members of the GDN. If you are unsure of the source or status of an asset that you'd like to add or have any confusion please reach out.
